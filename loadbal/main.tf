terraform {
    required_providers {
      azurerm = {
        source = "hashicorp/azurerm"
        version = "~>2.0"
      }
    }
  }
  provider "azurerm" {
    features {}
  }
  
  ### Creating the Raj-Team Resource Group
  
  # Create a resource group if it doesn't exist
  resource "azurerm_resource_group" "myterraformgroup" {
      name     = "LB-Rajtam"
      location = "eastus"
      tags = {
          environment = "GA tech Lab"
      }
  }
 


##################################
# Load balencer Creation
##################################
## Load balencer
  resource "azurerm_public_ip" "RajTeam_LB_IP" {
    name                = "PublicIPForLB"
    location            = var.location_name
    resource_group_name = azurerm_resource_group.myterraformgroup.name
    allocation_method   = "Static"
  }

resource "azurerm_lb" "RajTeam_LoadBalancer" {
    name                = "TestLoadBalancer"
    location            = var.location_name
    resource_group_name = azurerm_resource_group.myterraformgroup.name
  
    frontend_ip_configuration {
      name                 = "PublicIPAddress"
      public_ip_address_id = azurerm_public_ip.RajTeam_LB_IP.id
    }
  }

resource "azurerm_lb_probe" "RajTeam_probe" {
  resource_group_name         = azurerm_resource_group.myterraformgroup.name
  loadbalancer_id             = azurerm_lb.RajTeam_LoadBalancer.id
  name                       = "Rajtamprobe"
  protocol = "TCP"
  port                       = 80
}

 resource "azurerm_lb_rule" "redteam-lb-rule" {
  resource_group_name            = azurerm_resource_group.myterraformgroup.name
  loadbalancer_id               = azurerm_lb.RajTeam_LoadBalancer.id
  name                           = "LBRule"
  protocol                       = "Tcp"
  frontend_port                  = 80
  backend_port                   = 80
  probe_id                       = azurerm_lb_probe.RajTeam_probe.id
  frontend_ip_configuration_name = "PublicIPAddress"
}