#!/bin/bash
wget https://gitlab.com/rajeshthella/gatech-homework/-/raw/main/pentest.yml
wget https://gitlab.com/rajeshthella/gatech-homework/-/raw/main/hosts
wget https://gitlab.com/rajeshthella/gatech-homework/-/raw/main/ansible.cfg
sudo docker start `sudo docker container list -a | awk '{print $1}' | grep -v CONTAINER`
sudo docker cp hosts `sudo docker container list -a | awk '{print $1}' | grep -v CONTAINER`:/etc/ansible/hosts
sudo docker cp ansible.cfg `sudo docker container list -a | awk '{print $1}' | grep -v CONTAINER`:/etc/ansible/ansible.cfg
sudo docker cp pentest.yml `sudo docker container list -a | awk '{print $1}' | grep -v CONTAINER`:/etc/ansible/pentest.yml
sudo docker exec `sudo docker container list -a | awk '{print $1}' | grep -v CONTAINER` ansible webservers -m ping
sudo docker exec `sudo docker container list -a | awk '{print $1}' | grep -v CONTAINER` ansible-playbook /etc/ansible/pentest.yml

