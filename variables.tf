variable "location_name" {
  description = "location of the resource group"
  type        = string
  default     = "eastus"
}

variable "os_server" {
  description = "Serveranme"
  type        = string
  default     = "UbuntuServer"

}

variable "os_publisher" {
  description = "publisher name"
  type        = string
  default     = "Canonical"
}

variable "os_sku" {
  description = "OS sku  name"
  type        = string
  default     = "18.04-LTS"
}

variable "os_version" {
  description = "OS version"
  type        = string
  default     = "latest"
}
 
 variable "os_username" {
  description = "username"
  type        = string
  default     = "azadmin"
}

 variable "allocation" {
  description = "allocation methos"
  type        = string
  default     = "Dynamic"
}