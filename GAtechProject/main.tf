terraform {
    required_providers {
      azurerm = {
        source = "hashicorp/azurerm"
        version = "~>2.0"
      }
    }
  }
  provider "azurerm" {
    features {}
  }
  
  ### Creating the Red-Team Resource Group
  
  # Create a resource group if it doesn't exist
  resource "azurerm_resource_group" "myterraformgroup1" {
      name     = "GATech_Project1_Red-Team"
      location = "eastus"
      tags = {
          environment = "GA tech Lab"
      }
  }

  
  ## creating app service GATech_PublicIP_Load_Balancer

  resource "azurerm_app_service_plan" "Project1Plan" {
  name                = "example-appserviceplan"
  location            = azurerm_resource_group.myterraformgroup1.location
  resource_group_name = azurerm_resource_group.myterraformgroup1.name

  sku {
    tier = "Standard"
    size = "S1"
  }
}

resource "azurerm_app_service" "example" {
  name                = "example-app-service"
  location            = azurerm_resource_group.myterraformgroup1.location
  resource_group_name = azurerm_resource_group.myterraformgroup1.name
  app_service_plan_id = azurerm_app_service_plan.Project1Plan.id

}