terraform {
    required_providers {
      azurerm = {
        source = "hashicorp/azurerm"
        version = "~>2.0"
      }
    }
  }
  provider "azurerm" {
    features {}
  }
  
  ### Creating the Ga Tech ELK Project Resource Group
  
  # Create a resource group if it doesn't exist
  resource "azurerm_resource_group" "GATECH_ELK_group" {
      name     = "ELK_Project"
      location = "eastus"
      tags = {
          environment = "GATECH_ELK_Lab"
      }
  }
 

  #### Availability set up
  resource "azurerm_availability_set" "ELK_Avaiability" {
    name                = "availabiltaset"
    location            = var.location_name
    resource_group_name = azurerm_resource_group.GATECH_ELK_group.name
    tags = {
      environment = "GATECH_ELK_Lab"
    }
  }
  
  
  # Create virtual network
  resource "azurerm_virtual_network" "myterraformnetwork" {
      name                ="ELK_network" 
      address_space       = ["10.0.0.0/16"]
      location            = var.location_name
      resource_group_name = azurerm_resource_group.GATECH_ELK_group.name
  
      tags = {
          environment = "GATECH_ELK_Lab"
      }
  }
  
  resource "azurerm_network_security_group" "myterraformnsg" {
      name                = "myNetworkSecurityGroup"
      location            = var.location_name
      resource_group_name = azurerm_resource_group.GATECH_ELK_group.name
  
      security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
      }
    security_rule {
        name                       = "Web"
        priority                   = 1002
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "80"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
      }
    security_rule {
        name                       = "Web"
        priority                   = 1002
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "80"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
      }
      tags = {
          environment = "GATECH_ELK_Lab"
      }
  }
  
  # Create subnet
  resource "azurerm_subnet" "ELK_Subnet" {
      name                 = "ELK_Subnet1"
      resource_group_name  = azurerm_resource_group.GATECH_ELK_group.name
      virtual_network_name = azurerm_virtual_network.myterraformnetwork.name
      address_prefixes       = ["10.0.1.0/28"]
  }
  
  # Create public IPs
  resource "azurerm_public_ip" "myterraformpublicip" {
      name                         = "ELKPublicIP"
      location                     = var.location_name
      resource_group_name          = azurerm_resource_group.GATECH_ELK_group.name
      allocation_method            = "Dynamic"
  
      tags = {
          environment = "GATECH_ELK_Lab"
      }
  }
  
  resource "azurerm_network_interface" "myterraformnic" {
      name                      = "ELKNIC"
      location                  = var.location_name
      resource_group_name       = azurerm_resource_group.GATECH_ELK_group.name
  
      ip_configuration {
          name                          = "myNicConfiguration"
          subnet_id                     = azurerm_subnet.ELK_Subnet.id
          private_ip_address_allocation = var.allocation
          public_ip_address_id          = azurerm_public_ip.myterraformpublicip.id
      }
  
      tags = {
          environment = "GATECH_ELK_Lab"
      }
  }

  resource "azurerm_network_interface" "privateNIC" {
      count                     = 2
      name                      = "Privatnicip${count.index}"
      location                  = var.location_name
      resource_group_name       = azurerm_resource_group.GATECH_ELK_group.name
      
      ip_configuration {
          name                          = "mprovateyNicConfiguration"
          subnet_id                     = azurerm_subnet.ELK_Subnet.id
          private_ip_address_allocation = var.allocation
      }
  }  

resource "azurerm_ssh_public_key" "techlabkey" {
      name                = "ELK_sshkey"
        resource_group_name   = azurerm_resource_group.GATECH_ELK_group.name
        location            = var.location_name
        public_key          = file("~/.ssh/id_rsa.pub")
  }
  
  
##Web VMs Creation
resource "azurerm_linux_virtual_machine" "ApplicationServers" {
        count                 = 2
        name                  = "ELKProjectWebServer${count.index}"
        location              = var.location_name
        resource_group_name   = azurerm_resource_group.GATECH_ELK_group.name
        network_interface_ids = [element(azurerm_network_interface.privateNIC.*.id,count.index)]
        availability_set_id    = azurerm_availability_set.ELK_Avaiability.id
        size                  = "Standard_DS1_v2"

    os_disk {
        name                  = "myOsDisk${count.index}"
        caching               = "ReadWrite"
        storage_account_type  = "Premium_LRS"
    }

    source_image_reference {
        publisher             = var.os_publisher
        offer                 = var.os_server
        sku                   = var.os_sku
        version               = var.os_version
    }
    admin_ssh_key {
       username                 = var.os_username
       public_key               = azurerm_ssh_public_key.techlabkey.public_key
    }
       computer_name            = "webvms"
       admin_username           = var.os_username
       disable_password_authentication = true
}

#### Attacking Box Creation
resource "azurerm_linux_virtual_machine" "ELKAttachingMachine" {
    name                        = "ELKAttachingserver"
    location                    = var.location_name
    resource_group_name         = azurerm_resource_group.GATECH_ELK_group.name
    network_interface_ids       = [azurerm_network_interface.myterraformnic.id]
    size                        = "Standard_DS1_v2"

    os_disk {
        caching                 = "ReadWrite"
        storage_account_type    = "Premium_LRS"
    }

    source_image_reference {
        publisher             = var.os_publisher
        offer                 = var.os_server
        sku                   = var.os_sku
        version               = var.os_version
    }
    admin_ssh_key {
       username                 = var.os_username
       public_key = azurerm_ssh_public_key.techlabkey.public_key
    }
#      computer_name  = "ELK_Attaching_server"
       admin_username = var.os_username
       disable_password_authentication = true

   provisioner "remote-exec" {
       inline = [
         "sudo apt update",
         "sudo apt install docker.io -y",
         "sudo systemctl start docker",
         "sudo docker pull cyberxsecurity/ansible",
         "sudo docker run -it -d cyberxsecurity/ansible:latest bash",
        "sudo docker start `sudo docker container list -a | awk '{print $1}' | grep -v CONTAINER`"
     ]
    connection {
        type = "ssh"
        user = var.os_username
        host = azurerm_linux_virtual_machine.ELKAttachingMachine.public_ip_address
        private_key = file("~/.ssh/id_rsa")
        }
     }
}

  # Create public IPs
  resource "azurerm_public_ip" "myterraformpublicip1" {
      name                         = "ELKPublicIP1"
      location                     = var.location_name
      resource_group_name          = azurerm_resource_group.GATECH_ELK_group.name
      allocation_method            = "Dynamic"
  
      tags = {
          environment = "GATECH_ELK_Lab"
      }
  }

  resource "azurerm_network_interface" "myterraformnic1" {
      name                      = "ELKNIC1"
      location                  = var.location_name
      resource_group_name       = azurerm_resource_group.GATECH_ELK_group.name
  
      ip_configuration {
          name                          = "ELKNICNicConfiguration1"
          subnet_id                     = azurerm_subnet.ELK_Subnet.id
          private_ip_address_allocation = var.allocation
          public_ip_address_id          = azurerm_public_ip.myterraformpublicip1.id
      }
  
      tags = {
          environment = "GATECH_ELK_Lab"
      }
  }



#### ELK Monitoring Box Creation
resource "azurerm_linux_virtual_machine" "ELKMonitoringMachine" {
    name                        = "ELKMonitoringserver"
    location                    = var.location_name
    resource_group_name         = azurerm_resource_group.GATECH_ELK_group.name
    network_interface_ids       = [azurerm_network_interface.myterraformnic1.id]
    size                        = "Standard_DS1_v2"

    os_disk {
        name                    = "myOsDisk"
        caching                 = "ReadWrite"
        storage_account_type    = "Premium_LRS"
    }

    source_image_reference {
        publisher             = var.os_publisher
        offer                 = var.os_server
        sku                   = var.os_sku
        version               = var.os_version
    }
    admin_ssh_key {
       username                 = var.os_username
       public_key = azurerm_ssh_public_key.techlabkey.public_key
    }
       computer_name  = "ELKMonitoringserver"
       admin_username = var.os_username
       admin_password = "Cybertech!"
}
##################################
# Load balencer Creation
##################################
## Load balencer
  resource "azurerm_public_ip" "GATechTeam_LB_IP" {
    name                = "GATech_PublicIP_Load_Balancer"
    location            = var.location_name
    resource_group_name = azurerm_resource_group.GATECH_ELK_group.name
    allocation_method   = "Static"
  }

resource "azurerm_lb" "ELK_LoadBalancer" {
    name                = "ELK_LoadBalancerr"
    location            = var.location_name
    resource_group_name = azurerm_resource_group.GATECH_ELK_group.name
  
    frontend_ip_configuration {
      name                 = "PublicIPAddress"
      public_ip_address_id = azurerm_public_ip.GATechTeam_LB_IP.id
    }
  }

resource "azurerm_lb_probe" "GATechTeam_probe" {
  resource_group_name         = azurerm_resource_group.GATECH_ELK_group.name
  loadbalancer_id             = azurerm_lb.ELK_LoadBalancer.id
  name                       = "GATechtamprobe"
  protocol = "TCP"
  port                       = 80
}

 resource "azurerm_lb_rule" "ELK-lb-rule" {
  resource_group_name            = azurerm_resource_group.GATECH_ELK_group.name
  loadbalancer_id                = azurerm_lb.ELK_LoadBalancer.id
  name                           = "LBRule"
  protocol                       = "Tcp"
  frontend_port                  = 80
  backend_port                   = 80
  frontend_ip_configuration_name = "PublicIPAddress"
}
